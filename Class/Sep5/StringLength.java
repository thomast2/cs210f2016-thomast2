class StringLength {
    public static void main(String args[]){
        //quick and easy
        String str = "MIPS is awesome";
        int length = str.length();
        System.out.println(length);

        //MIPS Style
        int currentPosition = 0;
        while (str.charAt(currentPosition)!=0){
            currentPosition++;
        }
        System.out.println(currentPosition);
    }
}
