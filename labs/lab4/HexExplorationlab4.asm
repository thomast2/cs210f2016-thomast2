.data
	hello:		.asciiz		"Hello world!\n"
	number1:	.word		42
	number2:	.half           21
	number3:	.half		1701
	number4:	.byte		73
	number5:	.half		-1701
	number6:	.byte		127
	number7:	.word		65536
	sum:  		.word 		0
	difference:	.word		0
	
	#######################
	#  ADDRESS   #  DATA  #
	#######################
	# 0x10010010 #       42 #
	# 0x10010011 #       42 #
	# 0x10010012 #       42 #
	# 0x10010013 #       42 #
	# 0x10010014 #   21    #
	# 0x10010015 #     21  #
	# 0x10010016 #  1701 #
	# 0x10010017 # 1701  #
	# 0x10010018 #     73  #
	# 0x10010019 #-1701 #
	# 0x1001001a # -1701#
	# 0x1001001b #    127#
	# 0x1001001c  #65536#
	# 0x1001001d #65536#
	# 0x1001001e #65536#
	# 0x1001001f  #65536#
	# 0x10010020 #65536#
	# 0x10010021 #        #
	# 0x10010022 #        #
	# 0x10010023 #        #
	# 0x10010024 #        #
	# 0x10010025 #        #
	# 0x10010026 #        #
	# 0x10010027 #        #
	#######################


.text

	la $a0, hello
	li $v0, 4
	syscall
	lw $t1, number1 			#load .word number1 into temporary address 1
	lh $t2, number2				#load .word number2 into temporary address 2
	lh $t3, number3				#load .word number3 into temporary address 3
	lw $t4, number4				#load .word number4 into temporary address 4
	lh $t5, number5				#load .word number5 into temporary address 5
	lb $t6, number6				#load .word number6 into temporary address 6
	lb $t7, number7				#load .word number7 into temporary address 7
	
	add $t8,$t3,$t5				#add numbers from register $t3 and register $t5 and store them in register $t8
	sub $t9,$t5,$t3			        #subtract number from register $t3 from number in register $t5 and store them in register $t9

	sb $t8,sum				#stores byte value in register $t8 into "sum"
	sb $t9,difference			#stores byte value in register $t9 into "difference"
	
	li $v0, 10
	syscall
