.data
	multiplicand:	.word	2
	multiplier:	.word	5
	
	answer: 		.asciiz "The answer is: \n" 
	
	
.text
	
	la $t0, multiplicand			#load multiplicand to $t0
	la $t1, multiplier			#load muliplier to address $t1
	li $t7, 0				#load immediate value of 0 into $t7 to become the product register
	lw $t2, 0($t1)				#loads word value in register $t0 into $t2
	lw $t3, 0($t0)				#loads word value in register $t1 into $t3
	
	lw $t5, 0($t1)
	
	li $t4, 0				#Creates a counter instance
LoopTop: 
	addi $t4, $t4,1				#Counter begins 
	beq $t4,32, End				#the counter breaks to the end when it reaches 32nd rep
	
	andi $t0, $t5, 0x1

	beq $t5, 0, Bottom			#If the right most value in multiplier equals zero break to bottom
	
	add $t7,$t2,$t3 			# add both values and store in product register
	
	sll $t3,$t3,1				#Shift value in register $t3 and store it in the same register
	srl $t2,$t2,1				#store value in register $t2 and store it in same register
	srl $t5, $t5, 1				#shift value in $t5 and store it in the same register
	
Bottom:

	sll $t3,$t3,1				#Shift value in register $t3 and store it in the same register
	srl $t2,$t2,1				#store value in register $t2 and store it in same register
	
	j LoopTop				#jump back to loopTop
End: 
	
	
	la $a0, answer				#syscall command for printing a string
	li $v0, 4			
	syscall				

				
	li $v0, 1				#syscall command for printing an integer
	move $a0, $t7			
	syscall	
	
	li $v0, 10				#quit program
	syscall					
	
	
	
	
