#include <stdio.h>
#include <math.h>

char hex[8];                     // char array of size 8 to convert input to hex
char binary[32];                 // char array of size 32 to convert input to binary
char hexDigits[16] = {
    '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
};                               // Hexdigits array to mask the bitmasked values to proper Hex representation
unsigned bitMask[8] = {
    0xF0000000,0x0F000000,0x00F00000,0x000F0000,0x0000F000,0x000000F00,0x0000000F0,0x0000000F
};                               // Array Mask each 4 bits of input to be converted to hex
unsigned hexMask[32] = {
1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,
524288,
1048576,
2097152,
4194304,
8388608,
16777216,
33554432,
67108864,
134217728,
268435456,
536870912,
1073741824,
2147483648,
};                                // hexMask array that represents powers of 2

unsigned signMask = 0x80000000;   //signMask to mask out the sign of binary number
unsigned expMask = 0x7F800000;    //expMask to mask out the next eight bits after exponent
unsigned sigMask = 0x007FFFFF;    //sigMask to mask out remaining 23 bits

/**
 *Converts decimal input to hexadecimal representation
 */
void decimalToHex(unsigned bits){
    int i;
    for(i=0; i<8; i++){           //loop converts every four bits to proper hex representation
        unsigned x = bits & bitMask[i]; //bit mask input according to i
        x = x >> (7-i)*4;         // shift bits according to ith index of bitmask
        hex[i] = hexDigits[x];    //hex array contains hexadecimal representation
}
}
/**
 *Converts input to binary form
 */
void toBinary (unsigned bin){
    int i;
    for(i=0; i<32; i++){          // mask input for each bit and either prints a 1 or 0
        unsigned z = bin & hexMask[31-i]; //bit mask input according to 31st index minus i
    if(z==0){
        binary[i] = '0';
    }
    else{
        binary[i] = '1';
    }
    }
}
/**
 *Finds sign according to first bit value
 */
void toSign(unsigned a){
    unsigned x = a & signMask; //mask input to represent only leading bit
    x = x >> 31;               //shift bit all the way to the right

    if(x == 0){
         printf("Sign: +\n");
    }
    else{
        printf("Sign: -\n");
    }
}
/**
 *finds exponent according to the 24-31st bits
 */
void toExponent(unsigned exp){
    int exponent;
    unsigned x = exp & expMask; //mask input to represent only 24-31st bits
    x = x >> 23;                //shift 23 bits to the right
    exponent = x - 127;         //subtract 127 from new x value
    printf("Exponent: 2^%d\n", exponent);
}
/**
 *finds significand by dividing value of first 23 bits
 *by 2^23 power.
 */
void toSignificand(unsigned sig){
    float significand;
    unsigned x = sig & sigMask; //mask input to represent only first 23 bits
    significand = (x/(pow(2,23))) +1; //divide x by power of 2^23 and add 1 to that value to attain significand representation
    printf("Significand: %f\n", significand);
}

/**
 *Main Method
 *Calls instances of above function methods.
 */
int main() {
    unsigned input;
    printf("Enter a float value: ");
    scanf("%f", &input);
    decimalToHex(input);
    printf("In hex: 0x%s\n", hex);
    toBinary(input);
    printf("In Binary: %.32s\n", binary);
    toSign(input);
    toExponent(input);
    toSignificand(input);
}




