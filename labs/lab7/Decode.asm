.data
	request:		.asciiz	"Please enter a float value:\n"

	binary:		.space	35

	
	
.text
	la $a0, request 		#Loads string request into register to be printed out
	li $v0, 4			#The syscall command necessary for printing a string
	syscall				#executes the syscall
	li $v0, 6			#The syscall command necessary for reading in a float
	syscall				#Executes the syscall
	
	mfc1 $s0, $f0	
	move $a0,$s0
	li $v0, 34
	syscall
	
	la $t7, binary
	
	
	andi $s1,$s0, 0x80000000
	srl $s1, $s1, 31
	
	andi $s2, $s0, 0x7F800000
	srl $s2, $s2, 23
	
	andi $s3, $s0, 0x007FFFFF
	
	beqz $s1, sign0
	li $t1, '1'
	j endSign0
	
sign0:
	li $t1, '0'
	
endSign0:
	
	sb $t1, ($t7)
	addiu $t7, $t7, 1
	
	li $t1, ' '
	sb $t1, ($t7)
	addiu $t7,$t7, 1
	
	li $t3, 0x40000000
exp:
	
	beq $t3, 0x00400000, addSpace
	and $t4, $t3, $s0
	
	beqz $t4, exp0
	li $t5, '1'
	j endExp 
exp0:
	li $t5, '0'
endExp:
	sb $t5, ($t7)
	addiu $t7, $t7, 1

	
	srl $t3,$t3,1
	j exp
	
addSpace:
	li $t5, ' '
	sb $t5, ($t7)
	addiu $t7,$t7,1
	
	j significand
significand:
	
	beq $t3, 0x00000000, print
	
	li $s5, 0x00400000
	and $s4, $s5, $s0
	
	beqz $s4, sig0
	li $t6, '1'
	j endSig	
	
	
sig0:
	li $t6, '0'

endSig:
	sb $t6, ($t7)
	addiu $t7,$t7, 1
	
	srl $t3,$t3,1
	
	j significand 
	
signofNumber:
	bne $t1, '1', signOfNumber0
	li $s4, '-'
	j signEnd
signOfNumber0:
	li $t4, '+'
	j signEnd
signEnd:
	li $v0, 2
	move $a0, $t8
	syscall
	
print:
	li $t9, 1
	beq $t9, 35, exit
	
	lb $t8, binary($t9)
	
	addiu $t9,$t9,1
	
	li $v0, 11
	move $a0, $t8
	syscall
	
	j print
exit:
	li $v0, 10
	syscall
	
	
		




	


	
	
	
