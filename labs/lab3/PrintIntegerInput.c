#include <stdio.h>
#include <math.h>

//Method for creating a data table of four different calculations for x with a user inputted value 1 through 50.
int main() {

    int number;
    int i = 0;

    // printf() displays the formatted output
    printf("Enter an integer,x, 1 through 50:");

    // scanf() reads the formatted input and stores them
    scanf("%d", &number);


    // printf() displays the formatted output
    printf("You entered: %d\n", number);

    // printf() displays a line break in between the top row and results.
    printf("x\t SQRT\t 1/x^2\t 2^sqrt(x)\tx(x-1)\n---------------------------------------------\n");

    // while loop will print out calculations of each expression of x only for numbers 1 through 50
    // Data will be organized neatly in a table.
    while (i <= number){
        double column1 = sqrt(i);
        double column2 = 1/(pow(i,2));
        double column3 = pow(2,sqrt(i));
        double column4 = i*(i-1);

        printf("%d\t %.2f\t %.2f\t %.2f\t \t%.2f\n",i, column1, column2, column3, column4);
        i++;

    }

    return 0;

} //main
