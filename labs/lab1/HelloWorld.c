#include <stdio.h>// Loads input and output from library because C does not support it on its own
String myString = "Hello World!"
int main() { //Main function that returns an integer
    printf(myString); // Prints Hello World! with a new line
    return 0;// returns 0 if there are no errors in the code
} //main
