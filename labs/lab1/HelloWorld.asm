.data
	hello:
.asciiz
	"Hello world!\n"
.text
	#loading to the value $a0
	la $a0, hello
	#loads 4 into the corresponding register value List Immediate
	li $v0, 4
	#prints hello world calls the corresponding value
	syscall
	#loads 10 or a into the corresponding register value
	li $v0, 10
	#ends the program
	syscall