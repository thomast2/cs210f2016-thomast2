.data
	str:	.asciiz			"MIPS is awesome!"

	str2:	.asciiz			"The string length is:\n"
	
	length: 	.word 			0
.text

	la $t0, str			# t$0 holds the string address
	li $t1, 0			# 4t1 holds teh character count
	 
loopTop:					# Top of our loop
   	lb $t2, 0($t0)
   	
   	
   	bne $t2, $zero, notEqual	# jump to notEqual if things aren't equal
   	
   	sw $t1, length
   	
   	# found our end of string
   	li $v0, 4
   	la $a0, str2
   	syscall
   	
   	li $v0, 1			# settig syscall1
   	move $a0, $t1			# issuing the system call
   	syscall
   	
   	li $v0, 10			# setting syscall to 10
   	syscall				# issuing the system call
   		
   			
  notEqual: 
  	addi $t1, $t1, 1		# increment $t1
  	addi $t0, $t0, 1
  	j loopTop			# jump to top of the loop	
