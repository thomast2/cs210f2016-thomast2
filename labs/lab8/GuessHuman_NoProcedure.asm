.data
	welcome:		.asciiz		"Welcome to the Guessing Game, human!\nI am thinking of a number between 1 and 100\nSee if you can guess it!\n"
	yourGuess:	.asciiz		"Your first guess: "
	tooHigh:		.asciiz		"\nYour guess was too high!\n Your guess: "
	tooLow:		.asciiz		"\nYour guess was too low!\nYour guess: "
	correct:		.asciiz		"Your got it right! You got it in "
	correct2:		.asciiz		" tries! My number was: "
	playAgain:	.asciiz		"Would you like to play again (y/n)?"
	answer:		.space		256
.text

start:
	la $a0, welcome
	li $v0, 4
	syscall
	
	li $a1, 101
	li $v0, 42
	syscall
	
	move $s1, $a0 
	
	li $v0, 1
	syscall
	
	
	la $a0, yourGuess
	li $v0, 4
	syscall
	
	j loopTop
loopTop:
	li $v0, 5
	syscall
	
	move $s2,$v0
	
	beq $s2,$s1, bottom
	
	bgt $s2,$s1, greaterThan
	
	la $a0, tooLow
	li $v0, 4
	syscall
	
	j loopTop

greaterThan:
	la $a0, tooHigh
	li $v0, 4
	syscall
	
	j loopTop

bottom: 
	la $a0, correct
	li $v0, 4
	syscall
	
	la $a0, ($s7)
	li $v0, 1
	syscall
	
	la $a0, correct2
	li $v0, 4
	syscall
	
	la $a0, playAgain
	li $v0, 4
	syscall
	
	la $a0, answer
	li $a1, 3
	li $v0, 8
	syscall
	
	lb $t4, 0($a0)
	
	beq $t4, 'y', start
	
	li $v0, 10,
	syscall
	
	
	
	
	
	
	
	
	
	
	
