.data
	welcome:		.asciiz		"Welcome to the Guessing Game, human!\nI am thinking of a number between 1 and 100\nSee if you can guess it!\n"
	yourGuess:	.asciiz		"Your first guess: "
	tooHigh:		.asciiz		"\nYour guess was too high!\n Your guess: "
	tooLow:		.asciiz		"\nYour guess was too low!\nYour guess: "
	correct:		.asciiz		"You got it right! You got it in "
	correct2:		.asciiz		" tries\n"
	playAgain:	.asciiz		"Would you like to play again (y/n)?"

	
.text
TOP:
	jal WELCOME
	jal RANDOM
	jal USERINPUT
	
	li $s7, 0
	
	jal HIGHLOW
	
	
		
	li $v0, 10
	syscall
	
	
WELCOME:
	#prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	#procedure body
	la $a0, welcome
	li $v0, 4
	syscall
	
	# epilogue
    	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra

RANDOM:
	#prologue
	subi $sp, $sp, 16
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a1, 8($sp)
	sw $a0, 12($sp)
	
	
	#procedure body
	
	li $a1, 101
	li $v0, 42
	syscall
	
	move $s1, $a0 			#s1 global variable for random number

	# epilogue
    	lw $a0, 12($sp)
    	lw $a1, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 16
    	
    	# return
    	jr $ra
    	
 USERINPUT:
 	#prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
 	
 	#procedure body
 	la $a0, yourGuess
	li $v0, 4
	syscall
	
	# epilogue
    	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	#return
    	jr $ra

HIGHLOW:
 	
 	loopTop:
	li $v0, 5
	syscall
	
	move $s2,$v0
	

	addi $s7,$s7, 1
	
	beq $s2,$s1, bottom
	
	bgt $s2,$s1, greaterThan
	
	la $a0, tooLow
	li $v0, 4
	syscall
	
	j loopTop
	
	greaterThan:
	la $a0, tooHigh
	li $v0, 4
	syscall
	
	j loopTop
	
	bottom: 
	la $a0, correct
	li $v0, 4
	syscall
	
	la $a0, ($s7)
	li $v0, 1
	syscall
	
	la $a0, correct2
	li $v0, 4
	syscall
	
	la $a0, playAgain
	li $v0, 4
	syscall
	
	
	li $v0, 12
	syscall
	
	beq $v0, 'y', end

	# epilogue
    	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra

	
end:
	addi $sp,$sp,12
	j TOP

	
	
	
	

	
	
