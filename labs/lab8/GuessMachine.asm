.data
	welcome:		.asciiz		"Welcome to the Guessing Game, human!\nThink of a number between 1 and 100\nSee if I can guess it!\n"
	firstGuess:	.asciiz		"Let's see, my first guess will be.... \n"
	howDidIDO:	.asciiz		"\nHow did I do?"
	tooHigh:		.asciiz		"\nDarn, my guess was too high. My next guess is "
	tooLow:		.asciiz		"\nDrats, my guess was too low. My next guess is "
	correct:		.asciiz 	"\nYay, that only took me "
	correct2:		.asciiz		" tries! your number was "
	playAgain:	.asciiz		"\nCan I play again???(y/n)"
	hiLow:		.asciiz		"\nIf the answer is:\nHigher, enter: 1\nLower, enter: -1\nCorrect, enter: 0\n"
	

.text
TOP:					#Top of program
	jal WELCOME			# Jump to WELCOME label This is the welcome message
	jal NUMBER			# Jump to NUMBER where user chooses number for computer to guess
	
	li $s7, 101			#Loads highest possible guess to $s7
	li $s6, 0			#Loads lowest possible guess to $s6
	li $s5, 0			#Initialize tries counter
Redo:					#Retry guess
	jal GUESS			#The computers new guess using $s7 and $s6 as highest and lowest limits
	jal HIGHLOW			#Feedback on computer's guess
	
	li $v0, 10			#Ends program
	syscall
	
	
WELCOME:
	#prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	#procedure body
	la $a0, welcome			#Loads welcome message
	li $v0, 4
	syscall
	
	# epilogue
    	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra

NUMBER:
	#prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $v0, 4($sp)
	sw $a0, 8($sp)
	
	#procedure body
	li $v0, 5			#User input the number for computer to guess
	syscall
	
	move $s1, $v0
	
	la $a0, firstGuess		#Displays the computer's first guess
	li $v0, 4
	syscall
	
	#epilogue
	lw $a0, 8($sp)
	lw $v0, 4($sp)
	lw $ra, 0($sp)
	addi $sp,$sp, 12
	
	#return
	jr $ra
GUESS:

	#prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $v0, 8($sp)
	
	addi $s5,$s5,1			#The guess loop which takes the middle number of newest high and low limits
	
	add $t0, $s6, $s7
	div $t0,$t0, 2
	
	la $a0, ($t0)
	li $v0, 1
	syscall
	
	la $a0, hiLow
	li $v0, 4
	syscall
	
	move $s2, $a0
	
	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra
    	
HIGHLOW:				#Feedback loop telling computer if guess is high or low
					#Using input of 1,0, -1 to jump to corresponding loop
					#changing corresponding register to new limit if higher or lower
	#prologue
	subi $sp, $sp, 12
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $v0, 8($sp)
	
	li $v0, 5
	syscall
	
	move $s3, $v0
	
	beq $s3,1, greater
	beq $s3,0, right
	beq $s3,-1, lower
greater:
	
	la $a0, tooHigh
	li $v0, 4
	syscall
	
	move $s7, $t0
	
	j Redo
lower:
	la $a0, tooLow
	li $v0, 4
	syscall
	
	move $s6, $t0
	j Redo
right:

	la $a0, correct
	li $v0, 4
	syscall
	
	la $a0, ($s5)
	li $v0, 1
	syscall
	
	la $a0, correct2
	li $v0, 4
	syscall
	
	la $a0, ($s1)
	li $v0, 1
	syscall
	
	la $a0, playAgain
	li $v0, 4
	syscall
	
	
	li $v0, 12
	syscall
	
	beq $v0, 'y', end

	# epilogue
    	lw $a0, 8($sp)
    	lw $v0, 4($sp)
    	lw $ra, 0($sp)
    	addi $sp, $sp, 12
    	
    	# return
    	jr $ra

	
end:
	addi $sp,$sp,12
	j TOP

	

	
	
